import sys
def decode_message(header, encoded_message):
    key_to_char = {}
    keys = generate_keys()
    for i in range(len(header)):
        key_to_char[keys[i]] = header[i]
    decoded_message = ""
    key_length = int(encoded_message[:3], 2)
    i = 3
    while key_length != 0:
        while encoded_message[i:i+key_length] != "1"*key_length:
            key=encoded_message[i:i+key_length]
            decoded_message += key_to_char[key]
            i += key_length
        i += key_length
        key_length = int(encoded_message[i:i+3], 2)
        i+=3
    return decoded_message

def generate_keys():
    return [bin(j)[2:].zfill(i) for i in range(1, 8) for j in range(2**i - 1) if bin(j)[2:] != '1' * i]


header = " ".join(i for i in sys.argv[1:-1] if "0" not in i)
encoded_message = sys.argv[-1]
print(decode_message(header, encoded_message))
