def generate_keys():
    keys = [ ]
    for i in range(1,8):
        for j in range(2**i-1):
            if bin(j)[2:] != '1' * i :
                keys.append(bin(j)[2:].zfill(i))
    return keys

